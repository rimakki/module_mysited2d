<?php

/**
 * @file
 * Defines migrations.
 */

/**
 * Implements hook_migrate_api().
 */
function mysited2d_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'mysited2d_group' => array(
        'title' => t('MySiteD2D'),
      ),
    ),
  );
  return $api;
}
