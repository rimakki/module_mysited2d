<?php

/**
 * @file
 * Contains mappings for the Drupal 6 node migrations.
 */

class MyPageMigration extends DrupalNode6Migration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    // Enable the following lines to map legacy Drupal 6 upload module to Drupal "attachments" field.
    // $this->addFieldMapping('field_attachments', 'upload')->sourceMigration('MyFile');
    // $this->addFieldMapping('field_attachments:file_class')->defaultValue('MigrateFileFid');
    // $this->addFieldMapping('field_attachments:preserve_files')->defaultValue(TRUE);
    // $this->addFieldMapping('field_attachments:description', 'upload:description');
    // $this->addFieldMapping('field_attachments:display', 'upload:list');

    // Do not map these source fields.
    $this->addUnmigratedSources(array('revision', 'log', 'revision_uid'), t('DNM'));

  }
}
